const app = require('express')();       // express app
const http = require('http');
let socketIO = require('socket.io');    // socket io for message ... could replace with http2/sse(server-sent-events)
let GlobalVars = require('./state/GlobalVars');
const port = 9080;

/* routing and parameters */
const bodyParser = require('body-parser');
const apiRouters = require('./router');

/* user session */
const session = require('express-session');
const randomstring = require('randomstring');

try {

    app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }))
    app.use(bodyParser.text({ type: 'text/html' }))
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }));

    /* session */
    app.use(
        session({
            secret : randomstring.generate(),
            cookie: { maxAge: 60000 },
            resave: false,
            saveUninitialized: false
        })
    );
    /* *** Run the Server *** */
    app.use('/api', apiRouters);
    /* *** Index page redirection *** */
    app.get('/', (req, res) => {
        // res.sendFile(`${__dirname}/static/test.html`)
        res.sendFile(`${__dirname}/test/simpleClient.html`)
    });

    const httpServer = http.createServer(app);
    let socketIOInstance = socketIO(httpServer);

    socketIOInstance.on('connection', (socket) => {
        console.log('SOCKET.IO A USER CONNECTED');
        socket.on('create', (data) => {
            console.log('SOCKET.IO create called', socket);
            socket.join(data.room);
            socketIOInstance.emit('message', 'New people joined');
        });
        socket.on('join', (data) => {
            console.log('SOCKET.IO join called', data);
        })
        socket.emit('message', 'Hi');
    });
    GlobalVars.socketIO = socketIOInstance; 
    // Add to global, so the controllers can manage own actions like create, join ...
    httpServer.listen(port, () => {
        console.log(`Server Listening on the port ${port}`);
    })
} catch (e) {
    console.error('Failed to boot the server', e);
}