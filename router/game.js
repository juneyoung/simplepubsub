const Game = require('../model/Game.js');
const router = require('express').Router();
let GlobalVars = require('../state/GlobalVars');

router.post('/create', (req, res) => {
  console.log('create request body', req.body);
  // console.log('create request session', req.session);
  console.log('GenerateGameSokect');
  let game = new Game();
  let gameId = game.gameId;
  // console.log('Global shit ', GlobalVars.socketIO);
  // console.log('EventBus socket', GlobalVars)
  GlobalVars.socketIO.emit('create', {
    room: gameId
  });
  res.json({
    result : 'SUCCESS',
    game : game
  })
});

module.exports = router;