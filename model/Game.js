const RandomString = require('randomstring');

// export default 안됨. import 구문 사용 불가  
module.exports = class Game {
    constructor () {
        this.gameId = RandomString.generate();
        this.turn = 0;
        this.created = new Date();
    }

    getMeta () {
        return {
            gameId : this.gameId,
            turn : this.turn,
            created : this.created
        };
    }
}